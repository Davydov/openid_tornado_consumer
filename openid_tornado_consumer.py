import tornado.ioloop
import tornado.web
from tornado.escape import xhtml_escape

import openid
from openid.store import memstore
from openid.store.filestore import FileOpenIDStore
from openid.consumer import consumer
from openid.extensions import sreg
from openid.extensions import pape
from utils import Session, to_dict

globalstore = memstore.MemoryStore()

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('template.html', openid='')

class VerifyHandler(tornado.web.RequestHandler):
    def get(self):
        openid_url = self.request.arguments.get('openid_identifier', None)[0]
        if not openid_url:
            self.render('template.html', openid='',
                        error='Enter an OpenID Identifier to verify.')
            return

        immediate = 'immediate' in self.request.arguments
        use_sreg = 'use_sreg' in self.request.arguments
        use_pape = 'use_pape' in self.request.arguments
        use_stateless = 'use_stateless' in self.request.arguments

        if use_sreg:
            self.set_secure_cookie('use_sreg', 'True')
        
        if use_pape:
            self.set_secure_cookie('use_pape', 'True')
        
        oidconsumer = consumer.Consumer(session=Session(self, 'sessions/'),
                                        store=globalstore)
        try:
            request = oidconsumer.begin(openid_url)
        except consumer.DiscoveryFailure, exc:
            error_string = 'Error in discovery: %s' % \
                xhtml_escape(str(exc[0]))
            self.render('template.html',
                        openid=xhtml_escape(openid_url),
                        error=error_string)
        else:
            if request is None:
                error_string = \
                    'No OpenID services found for <code>%s</code>' % \
                    xhtml_escape(openid_url)
                self.render('template.html', openid=xhtml_escape(openid_url),
                            error=error_string)
            else:
                # Then, ask the library to begin the authorization.
                # Here we find out the identity server that will verify the
                # user's identity, and get a token that allows us to
                # communicate securely with the identity server.
                if use_sreg:
                    sreg_request = sreg.SRegRequest(
                        required=['nickname'], optional=['fullname', 'email'])
                    request.addExtension(sreg_request)
                
                if use_pape:
                    pape_request = pape.Request([pape.AUTH_PHISHING_RESISTANT])
                    request.addExtension(pape_request)

                trust_root = 'http://%s' % self.request.headers.get('Host')
                return_to = trust_root + '/proccess'
                if request.shouldSendRedirect():
                    redirect_url = request.redirectURL(
                        trust_root, return_to, immediate=immediate)
                    self.redirect(redirect_url)
                else:
                    form_html = request.htmlMarkup(
                        trust_root, return_to,
                        form_tag_attrs={'id':'openid_message'},
                        immediate=immediate)
                    self.write(form_html)

class ProccessHandler(tornado.web.RequestHandler):
    def get(self):
        oidconsumer = consumer.Consumer(session=Session(self, 'sessions/'),
                                        store=globalstore)
        url = 'http://%s%s' % (self.request.headers.get('Host'),
                               self.request.uri)
        info = oidconsumer.complete(to_dict(self.request.arguments), url)

        sreg_resp = None
        pape_resp = None
        error = None
        alert = None
        display_identifier = info.getDisplayIdentifier()

        use_sreg = self.get_secure_cookie('use_sreg')
        if use_sreg:
            self.clear_cookie('use_sreg')
        use_pape = self.get_secure_cookie('use_pape')
        if use_pape:
            self.clear_cookie('use_pape')

        if info.status == consumer.FAILURE and display_identifier:
            # In the case of failure, if info is non-None, it is the
            # URL that we were verifying. We include it in the error
            # message to help the user figure out what happened.
            fmt = "Verification of %s failed: %s"
            error = fmt % (xhtml_escape(display_identifier),
                             info.message)
        elif info.status == consumer.SUCCESS:
            # Success means that the transaction completed without
            # error. If info is None, it means that the user cancelled
            # the verification.
            # This is a successful verification attempt. If this
            # was a real application, we would do our login,
            # comment posting, etc. here.
            fmt = "You have successfully verified %s as your identity."
            alert = fmt % (xhtml_escape(display_identifier),)
            sreg_resp = sreg.SRegResponse.fromSuccessResponse(info)
            pape_resp = pape.Response.fromSuccessResponse(info)
            if info.endpoint.canonicalID:
                # You should authorize i-name users by their canonicalID,
                # rather than their more human-friendly identifiers.  That
                # way their account with you is not compromised if their
                # i-name registration expires and is bought by someone else.
                alert += ("  This is an i-name, and its persistent ID is %s"
                            % (xhtml.escape(info.endpoint.canonicalID),))
        elif info.status == consumer.CANCEL:
            # cancelled
            error = 'Verification cancelled'
        elif info.status == consumer.SETUP_NEEDED:
            if info.setup_url:
                error = '<a href=%s>Setup needed</a>' % (
                    xhtml_escape(info.setup_url),)
            else:
                # This means auth didn't succeed, but you're welcome to try
                # non-immediate mode.
                error = 'Setup needed'
        else:
            # Either we don't understand the code or there is no
            # openid_url included with the error. Give a generic
            # failure message. The library should supply debug
            # information in a log.
            error = 'Verification failed.'

        sreg_v = {}
        if sreg_resp:
            for k in sreg_resp:
                field_name = sreg.data_fields.get(k, k)
                value = xhtml_escape(sreg_resp[k].encode('UTF-8'))
                sreg_v[field_name] = value
        self.render('template.html', openid=display_identifier, 
                    alert=alert, error=error,
                    use_sreg=use_sreg, use_pape=use_pape,
                    sreg=sreg_v, pape=pape_resp)

application = tornado.web.Application([
    (r'/', MainHandler),
    (r'/verify', VerifyHandler),
    (r'/proccess', ProccessHandler),
], cookie_secret='uW6IoNfRRq2qp7pDod0s+wBAuL9ZJnMjal5WEGxrgIs=')

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
