from uuid import uuid4
from pickle import dump, load
from os.path import exists

class Session:
    def __init__(self, handler, directory):
        self.handler = handler
        uuid = self.handler.get_secure_cookie('uuid')
        if uuid:
            self.uuid = uuid
        else:
            self.uuid = str(uuid4())
            self.handler.set_secure_cookie('uuid', self.uuid)
        self.filename = directory + self.uuid + '.txt'
        if not exists(self.filename):
            dump({}, open(self.filename, 'w'))

    def __getitem__(self, key):
        self.get(key)

    def __setitem__(self, key, val):
        d = load(open(self.filename))
        d[key] = val
        dump(d, open(self.filename, 'w'))

    def __delitem__(self, key):
        d = load(open(self.filename))
        del(d[key])
        dump(d, open(self.filename, 'w'))

    def get(self, key, default=None):
        d = load(open(self.filename))
        if d.has_key(key):
            return d[key]
        else:
            return default

def to_dict(mdict):
    d = {}
    for key in mdict:
        d[key] = mdict[key][-1]
    return d
